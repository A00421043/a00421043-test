Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class TestModel
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=TestModel")
    End Sub

    Public Overridable Property Tables As DbSet(Of Table)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of Table)() _
            .Property(Function(e) e.firstName) _
            .IsFixedLength()
    End Sub
End Class
