﻿'  File ContactForm
'  Sample code was taken from:
'  'https://docs.microsoft.com/en-us/dotnet/visual-basic/developing-apps/programming/drives-directories-files/how-to-read-from-comma-delimited-text-files
' 
' Assignment #2
' Due Oct 11, 2017
'
' Requirement 1: Expand on this form to display information in database that displays the following fields
' First Name (TextBox)
' Last Name (TextBox)
' Street Number (TextBox)
' City (TextBox) 
' Province (TextBox)
' Country (TextBox)
' Postal Code  (TextBox)( https://stackoverflow.com/questions/16614648/canadian-postal-code-regex)
' Phone Number (TextBox)( http://www.visual-basic-tutorials.com/Tutorials/Strings/validate-phone-number-in-visual-basic.htm)
' email Address (TextBox)( https://stackoverflow.com/questions/1331084/how-do-i-validate-email-address-formatting-with-the-net-framework)
'
' Add Next and Prevous Buttons to navigate through the database ( handle index 0 and max index)
' Display the current primary key of the database in a textbox or label
' Add a Status TextBox and dispaly any formatting errors that are encoutered, 
' If multiple errors exist only show last one.

' Requirement 2: Expand on the below example to create a import the contents of the CSV file 
' created in Assignment1, read the data into entity classes and save data to database.  
' After import Next and Prev buttons should work.
'
' TODO for Dan - add example of how to save data
'
' Please always try to write clean And readable code
' Here Is a good reference doc http://ricardogeek.com/docs/clean_code.html  
' Submit to Bitbucket under Assignment2

Imports System.Data.OleDb
Imports System.IO
Imports System.Text.RegularExpressions

Public Class ContactForm

    Dim index As Integer = 0
    Dim currentIndex As String
    'hashtable sample code
    ' https://support.microsoft.com/en-ca/help/307933/how-to-work-with-the-hashtable-collection-in-visual-basic--net-or-in-v
    '
    Dim allData As New Dictionary(Of Integer, Customer)

    Dim RowIdxFirstName As Integer = 0
    'Public Property csvFile As String = "C:\Users\dpenny\Documents\Source\Repos\mcda5510_assignments\WindowsApp1\names.csv"

    'Uploading form textbox with data
    Private Sub UpdateDataForDB(key As Integer)
        Dim cust As Customer = allData(key)
        tb_index.Text = key
        txtFirstName.Text = cust.firstName
        txtFirstName.Text = cust.firstName
        txtLastName.Text = cust.lastName
        txtStreetNumber.Text = cust.streetNumber
        txtCity.Text = cust.city
        txtProvince.Text = cust.province
        txtCountry.Text = cust.country
        txtPostalCode.Text = cust.postalCode
        txtPhoneNumber.Text = cust.phoneNumber
        txtEmailId.Text = cust.emailAddress

        lblFirstNameErr.Visible = False
        lblLastNameErr.Visible = False
        lblStreetNumberErr.Visible = False
        lblCityErr.Visible = False
        lblProvinceErr.Visible = False
        lblCountryErr.Visible = False
        lblPostalCodeErr.Visible = False
        lblPhoneNumberErr.Visible = False
        lblEmailAddressErr.Visible = False
        txtStatus.Text = getError(cust)

    End Sub


    'Private Sub UpdateData(index As Integer)
    '    Dim personData As Customer = Nothing
    '    If allData.TryGetValue(index, personData) Then
    '        tb_index.Text = index
    '        txtFirstName.Text = personData.firstName
    '        txtLastName.Text = personData.lastName
    '        txtStreetNumber.Text = personData.streetNumber
    '        txtCity.Text = personData.city
    '        txtProvince.Text = personData.province
    '        txtCountry.Text = personData.country
    '        txtPostalCode.Text = personData.postalCode
    '        txtPhoneNumber.Text = personData.phoneNumber
    '        txtEmailId.Text = personData.emailAddress
    '    End If
    'End Sub

    'on load function of Form 
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'LoadFromCSVFile()
        LoadFromDatabase()

    End Sub

    'To load data into form
    Private Sub LoadFromDatabase()
        allData.Clear()
        Using context As New CustomerModel
            Try
                Dim firstCust = context.Customers.ToList.OrderBy(Function(x) x.Id).First()
                Dim listOfCustomers = context.Customers.ToList
                For Each cust As Customer In listOfCustomers
                    allData.Add(cust.Id, cust)

                Next
                UpdateDataForDB(firstCust.Id)
            Catch ex As Exception
                'MsgBox("No Customer is present")
            End Try

        End Using
    End Sub

    'Private Sub LoadFromCSVFile(csvFile As String)
    '    Using MyReader As New Microsoft.VisualBasic.
    '                          FileIO.TextFieldParser(
    '                            csvFile)
    '        MyReader.TextFieldType = FileIO.FieldType.Delimited
    '        MyReader.SetDelimiters(",")
    '        Dim currentRow As String()
    '        Dim rowNumber As Integer = 0
    '        While Not MyReader.EndOfData
    '            Try
    '                currentRow = MyReader.ReadFields()
    '                Dim rowData As New ArrayList
    '                Dim currentField As String
    '                For Each currentField In currentRow
    '                    rowData.Add(currentField)
    '                Next
    '                allData.Add(rowNumber, rowData)
    '                rowNumber = rowNumber + 1
    '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
    '                MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
    '            End Try
    '        End While
    '    End Using
    '    UpdateData(0)
    'End Sub




    'For selecting file from different location on system---Csv file select.--Populate Browse text box
    Private Sub ImportCSVFile()
        Dim importFile As String = ""

        Dim fd As OpenFileDialog = New OpenFileDialog()
        fd.Title = "Select CSV file to import"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            importFile = fd.FileName
            txtboxBrowse.Text = fd.FileName
            LoadToGrid(fd.FileName)
        Else

        End If

    End Sub
    'On click of browse button click
    Private Sub btnBrowser_Click(sender As Object, e As EventArgs) Handles btnBrowser.Click
        ImportCSVFile()
    End Sub

    'For generating data grid.
    Private Sub LoadToGrid(filePath As String)
        Dim file As New FileInfo(filePath)
        Dim connectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & file.DirectoryName
        Dim conn As New OleDbConnection(connectionString)

        Dim cmdSelect As New OleDbCommand("select * from " & file.Name, conn)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = cmdSelect

        Dim ds As New DataSet
        adapter1.Fill(ds, "DATA")

        CsvDataGrid.DataSource = ds.Tables(0).DefaultView

        conn.Close()
    End Sub

    'On click of upload button --Calling internally UploadData
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        UploadData(txtboxBrowse.Text)
        LoadFromDatabase()
    End Sub
    'To upload data from Csv to Database
    Private Sub UploadData(filePath As String)
        Dim file As New FileInfo(filePath)
        Dim connectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & file.DirectoryName
        Dim conn As New OleDbConnection(connectionString)

        Dim cmdSelect As New OleDbCommand("select * from " & file.Name, conn)

        Dim adapter1 As New OleDbDataAdapter
        adapter1.SelectCommand = cmdSelect

        Dim ds As New DataSet
        adapter1.Fill(ds, "DATA")
        Using context As New CustomerModel
            ' Dim cust As New Customer
            Dim count = 1

            'Dim Id = context.Customers.OrderByDescending(Function(x) x.Id).First().Id

            For Each Row As DataRow In ds.Tables(0).Rows
                Dim cust As New Customer
                'cust.Id = Id + count
                cust.firstName = Row(ds.Tables(0).Columns(0)).ToString()
                cust.lastName = Row(ds.Tables(0).Columns(1)).ToString()
                cust.streetNumber = Row(ds.Tables(0).Columns(2)).ToString()
                cust.city = Row(ds.Tables(0).Columns(3)).ToString()
                cust.province = Row(ds.Tables(0).Columns(4)).ToString()
                cust.country = Row(ds.Tables(0).Columns(5)).ToString()
                cust.postalCode = Row(ds.Tables(0).Columns(6)).ToString()
                cust.phoneNumber = Row(ds.Tables(0).Columns(7)).ToString()
                cust.emailAddress = Row(ds.Tables(0).Columns(8)).ToString()
                context.Customers.Add(cust)
                context.SaveChanges()
                'count = count + 1
            Next
            MsgBox("File uploaded successfully")
            'Me.Refresh()
        End Using
    End Sub
    'On Click event on previous button
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        'UpdateDataForDB(tb_index.Text - 1)
        Try
            Using context As New CustomerModel
                Dim Id = context.Customers.OrderBy(Function(x) x.Id).First().Id
                If tb_index.Text = Id Then
                    MsgBox("This is the first record in database")
                Else
                    UpdateDataForDB(Find_Prev_Index(tb_index.Text - 1))
                End If
            End Using
        Catch ex As Exception
            CheckForId()
        End Try
    End Sub

    'On click of Next button
    Private Sub NextButton_Click(sender As Object, e As EventArgs) Handles btn_next.Click

        Using context As New CustomerModel

            Try
                Dim Id = context.Customers.OrderByDescending(Function(x) x.Id).First().Id



                If tb_index.Text = Id Then
                    MsgBox("This is the Last record in database")
                Else
                    UpdateDataForDB(Find_Next_Index(tb_index.Text + 1))
                End If
            Catch ex As Exception
                CheckForId()
            End Try
        End Using
    End Sub

    'To find Next index if sequence is broken
    Private Function Find_Next_Index(index As Integer) As Integer
        Using context As New CustomerModel
            Try
                Dim nextCust = context.Customers.Single(Function(x) x.Id = index)
            Catch ex As System.InvalidOperationException
                index = index + 1
                Find_Next_Index(index)
            End Try
        End Using
        Return index
    End Function

    'To find prev Index if sequence is broken
    Private Function Find_Prev_Index(index As Integer) As Integer
        Using context As New CustomerModel
            Try
                Dim nextCust = context.Customers.Single(Function(x) x.Id = index)
            Catch ex As System.InvalidOperationException
                index = index - 1
                Find_Prev_Index(index)
            End Try
        End Using
        Return index
    End Function


    'To generate error message
    Private Function getError(cust As Customer) As String
        Dim errorString = ""
        If cust.emailAddress = "" Or IsEmail(cust.emailAddress) = False Then
            errorString = "Email address not valid"
            lblEmailAddressErr.Text = errorString
            lblEmailAddressErr.Visible = True
        ElseIf cust.city = "" Then
            errorString = "City can not be null."
            lblCityErr.Text = errorString
            lblCityErr.Visible = True
        ElseIf cust.phoneNumber = "" Or IsvalidPhoneNumber(cust.phoneNumber) = False Then
            errorString = "Phone Number is not valid"
            lblPhoneNumberErr.Text = errorString
            lblPhoneNumberErr.Visible = True

        ElseIf cust.streetNumber = "" Then
            errorString = "Street Number can not be null."
            lblStreetNumberErr.Visible = True
            lblStreetNumberErr.Text = errorString

        ElseIf cust.postalCode = "" Or CanBeValidCanadianPostalCode(cust.postalCode) = False Then
            errorString = "Postal code is not Valid."
            lblPostalCodeErr.Text = errorString
            lblPostalCodeErr.Visible = True
        ElseIf cust.country = "" Then
            errorString = "Country can not be null."
            lblCountryErr.Text = errorString
            lblCountryErr.Visible = True

        ElseIf cust.province = "" Then
            errorString = "Province can not be null."
            lblProvinceErr.Text = errorString
            lblProvinceErr.Visible = True
        ElseIf cust.firstName = "" Then
            errorString = "First name can not be null."
            lblFirstNameErr.Text = errorString
            lblFirstNameErr.Visible = True
        ElseIf cust.lastName = "" Then
            errorString = "Last name can not be null."
            lblLastNameErr.Text = errorString
            lblLastNameErr.Visible = True
        End If
        'If IsEmail(cust.emailAddress) = False Then
        '    errorString = "Email Address is Incorrect"
        '    lblEmailAddressErr.Text = errorString
        '    lblEmailAddressErr.Visible = True
        'End If

        'If IsvalidPhoneNumber(cust.phoneNumber) = False Then
        '    errorString = "PhoneNumber is Incorrect"
        '    lblPhoneNumber.Text = errorString
        '    lblPhoneNumber.Visible = True
        'End If
        Return errorString
    End Function


    Function IsEmail(ByVal email As String) As Boolean
        Return Regex.IsMatch(email, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")

    End Function


    Function IsvalidPhoneNumber(ByVal phone As String) As Boolean
        Dim phoneNumber As New Regex("\d{3}-\d{3}-\d{4}")
        If phoneNumber.IsMatch(phone) Then
            Return True
        End If
        Return False

    End Function

    Private Function CanBeValidCanadianPostalCode(postal_code As String) As Boolean
        Return Regex.IsMatch(postal_code.Replace(" ", "").ToUpper, "([A-Z]\d){3}")
    End Function

    Private Sub btnClearDbTable_Click(sender As Object, e As EventArgs) Handles btnClearDbTable.Click
        Using context As New CustomerModel
            context.Database.ExecuteSqlCommand("Truncate table Customer")
            context.SaveChanges()
            MsgBox("Database table has been cleared. Exiting Application")
            Application.Exit()
        End Using
    End Sub

    Private Sub tb_index_TextChanged(sender As Object, e As EventArgs) Handles tb_index.TextChanged
        Using context As New CustomerModel
            'context.Customers.SingleOrDefault(Function(x) tb_index.Text).Id
            Try
                UpdateDataForDB(tb_index.Text)
            Catch ex As Exception
                MsgBox("No record present with this id")
                tb_index.Text = currentIndex
            End Try

        End Using
    End Sub

    Sub CheckForId()

        Using context As New CustomerModel
            Dim listOfCustomer = context.Customers.ToList()
            If listOfCustomer.Count = 0 Then
                MsgBox("Please upload file first")
            ElseIf (tb_index.Text = "") Then
                MsgBox("Id can not be Null")
            End If
        End Using
    End Sub
End Class
