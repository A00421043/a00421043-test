﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ContactForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btn_next = New System.Windows.Forms.Button()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.tb_index = New System.Windows.Forms.TextBox()
        Me.lblPrimaryKey = New System.Windows.Forms.Label()
        Me.lblFirstName = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClearDbTable = New System.Windows.Forms.Button()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.lblCsvFileData = New System.Windows.Forms.Label()
        Me.CsvDataGrid = New System.Windows.Forms.DataGridView()
        Me.btnBrowser = New System.Windows.Forms.Button()
        Me.txtboxBrowse = New System.Windows.Forms.TextBox()
        Me.lblForImport = New System.Windows.Forms.Label()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.lblStreetNumber = New System.Windows.Forms.Label()
        Me.txtStreetNumber = New System.Windows.Forms.TextBox()
        Me.lblPhoneNumber = New System.Windows.Forms.Label()
        Me.txtPhoneNumber = New System.Windows.Forms.TextBox()
        Me.lblPostalcode = New System.Windows.Forms.Label()
        Me.txtPostalCode = New System.Windows.Forms.TextBox()
        Me.lblCountry = New System.Windows.Forms.Label()
        Me.txtCountry = New System.Windows.Forms.TextBox()
        Me.lblProvince = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtEmailId = New System.Windows.Forms.TextBox()
        Me.txtProvince = New System.Windows.Forms.TextBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.tltipProvince = New System.Windows.Forms.ToolTip(Me.components)
        Me.tltpBtnPrev = New System.Windows.Forms.ToolTip(Me.components)
        Me.tltpBtnNext = New System.Windows.Forms.ToolTip(Me.components)
        Me.tltpBtnBrowse = New System.Windows.Forms.ToolTip(Me.components)
        Me.tltpBtnUpload = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblStreetNumberErr = New System.Windows.Forms.Label()
        Me.lblProvinceErr = New System.Windows.Forms.Label()
        Me.lblFirstNameErr = New System.Windows.Forms.Label()
        Me.lblLastNameErr = New System.Windows.Forms.Label()
        Me.lblCityErr = New System.Windows.Forms.Label()
        Me.lblCountryErr = New System.Windows.Forms.Label()
        Me.lblPostalCodeErr = New System.Windows.Forms.Label()
        Me.lblPhoneNumberErr = New System.Windows.Forms.Label()
        Me.lblEmailAddressErr = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CsvDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_next
        '
        Me.btn_next.Location = New System.Drawing.Point(477, 319)
        Me.btn_next.Name = "btn_next"
        Me.btn_next.Size = New System.Drawing.Size(75, 23)
        Me.btn_next.TabIndex = 0
        Me.btn_next.Text = "Next"
        Me.tltpBtnNext.SetToolTip(Me.btn_next, "Click to get next customer data")
        Me.btn_next.UseVisualStyleBackColor = True
        '
        'txtFirstName
        '
        Me.txtFirstName.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.Location = New System.Drawing.Point(110, 79)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(276, 28)
        Me.txtFirstName.TabIndex = 1
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(381, 319)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(76, 23)
        Me.btnPrev.TabIndex = 2
        Me.btnPrev.Text = "Prev"
        Me.tltpBtnPrev.SetToolTip(Me.btnPrev, "Click to get previous customer data")
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'tb_index
        '
        Me.tb_index.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_index.Location = New System.Drawing.Point(110, 22)
        Me.tb_index.Name = "tb_index"
        Me.tb_index.Size = New System.Drawing.Size(58, 28)
        Me.tb_index.TabIndex = 3
        '
        'lblPrimaryKey
        '
        Me.lblPrimaryKey.AutoSize = True
        Me.lblPrimaryKey.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrimaryKey.Location = New System.Drawing.Point(12, 25)
        Me.lblPrimaryKey.Name = "lblPrimaryKey"
        Me.lblPrimaryKey.Size = New System.Drawing.Size(82, 18)
        Me.lblPrimaryKey.TabIndex = 4
        Me.lblPrimaryKey.Text = "Primary Key"
        '
        'lblFirstName
        '
        Me.lblFirstName.AutoSize = True
        Me.lblFirstName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(12, 83)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(73, 18)
        Me.lblFirstName.TabIndex = 5
        Me.lblFirstName.Text = "First name"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnClearDbTable)
        Me.Panel1.Controls.Add(Me.btnUpload)
        Me.Panel1.Controls.Add(Me.lblCsvFileData)
        Me.Panel1.Controls.Add(Me.CsvDataGrid)
        Me.Panel1.Controls.Add(Me.btnBrowser)
        Me.Panel1.Controls.Add(Me.txtboxBrowse)
        Me.Panel1.Controls.Add(Me.lblForImport)
        Me.Panel1.Location = New System.Drawing.Point(63, 349)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(844, 269)
        Me.Panel1.TabIndex = 6
        '
        'btnClearDbTable
        '
        Me.btnClearDbTable.Location = New System.Drawing.Point(473, 241)
        Me.btnClearDbTable.Name = "btnClearDbTable"
        Me.btnClearDbTable.Size = New System.Drawing.Size(156, 23)
        Me.btnClearDbTable.TabIndex = 35
        Me.btnClearDbTable.Text = "Clear Database Table/Exit Application"
        Me.btnClearDbTable.UseVisualStyleBackColor = True
        '
        'btnUpload
        '
        Me.btnUpload.Location = New System.Drawing.Point(392, 241)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(75, 23)
        Me.btnUpload.TabIndex = 11
        Me.btnUpload.Text = "Upload"
        Me.tltpBtnUpload.SetToolTip(Me.btnUpload, "Click to upload data to database")
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'lblCsvFileData
        '
        Me.lblCsvFileData.AutoSize = True
        Me.lblCsvFileData.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCsvFileData.Location = New System.Drawing.Point(9, 83)
        Me.lblCsvFileData.Name = "lblCsvFileData"
        Me.lblCsvFileData.Size = New System.Drawing.Size(102, 19)
        Me.lblCsvFileData.TabIndex = 10
        Me.lblCsvFileData.Text = "CSV FILE DATA"
        '
        'CsvDataGrid
        '
        Me.CsvDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CsvDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CsvDataGrid.Location = New System.Drawing.Point(-1, 105)
        Me.CsvDataGrid.Name = "CsvDataGrid"
        Me.CsvDataGrid.Size = New System.Drawing.Size(844, 116)
        Me.CsvDataGrid.TabIndex = 9
        '
        'btnBrowser
        '
        Me.btnBrowser.Location = New System.Drawing.Point(584, 42)
        Me.btnBrowser.Name = "btnBrowser"
        Me.btnBrowser.Size = New System.Drawing.Size(97, 23)
        Me.btnBrowser.TabIndex = 8
        Me.btnBrowser.Text = "Browse"
        Me.tltpBtnBrowse.SetToolTip(Me.btnBrowser, "Click here to select csv file from your system")
        Me.btnBrowser.UseVisualStyleBackColor = True
        '
        'txtboxBrowse
        '
        Me.txtboxBrowse.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxBrowse.Location = New System.Drawing.Point(7, 37)
        Me.txtboxBrowse.Name = "txtboxBrowse"
        Me.txtboxBrowse.Size = New System.Drawing.Size(557, 28)
        Me.txtboxBrowse.TabIndex = 7
        '
        'lblForImport
        '
        Me.lblForImport.AutoSize = True
        Me.lblForImport.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblForImport.Location = New System.Drawing.Point(9, 9)
        Me.lblForImport.Name = "lblForImport"
        Me.lblForImport.Size = New System.Drawing.Size(128, 19)
        Me.lblForImport.TabIndex = 0
        Me.lblForImport.Text = "CSV File to Import:"
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(13, 141)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(70, 18)
        Me.lblLastName.TabIndex = 8
        Me.lblLastName.Text = "Last name"
        '
        'txtLastName
        '
        Me.txtLastName.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(110, 135)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(276, 28)
        Me.txtLastName.TabIndex = 7
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(15, 247)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(32, 18)
        Me.lblCity.TabIndex = 12
        Me.lblCity.Text = "City"
        '
        'txtCity
        '
        Me.txtCity.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCity.Location = New System.Drawing.Point(111, 237)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(276, 28)
        Me.txtCity.TabIndex = 11
        '
        'lblStreetNumber
        '
        Me.lblStreetNumber.AutoSize = True
        Me.lblStreetNumber.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStreetNumber.Location = New System.Drawing.Point(11, 190)
        Me.lblStreetNumber.Name = "lblStreetNumber"
        Me.lblStreetNumber.Size = New System.Drawing.Size(100, 18)
        Me.lblStreetNumber.TabIndex = 10
        Me.lblStreetNumber.Text = "Street Number"
        '
        'txtStreetNumber
        '
        Me.txtStreetNumber.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStreetNumber.Location = New System.Drawing.Point(111, 186)
        Me.txtStreetNumber.Name = "txtStreetNumber"
        Me.txtStreetNumber.Size = New System.Drawing.Size(276, 28)
        Me.txtStreetNumber.TabIndex = 9
        '
        'lblPhoneNumber
        '
        Me.lblPhoneNumber.AutoSize = True
        Me.lblPhoneNumber.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhoneNumber.Location = New System.Drawing.Point(474, 186)
        Me.lblPhoneNumber.Name = "lblPhoneNumber"
        Me.lblPhoneNumber.Size = New System.Drawing.Size(102, 18)
        Me.lblPhoneNumber.TabIndex = 23
        Me.lblPhoneNumber.Text = "Phone Number"
        '
        'txtPhoneNumber
        '
        Me.txtPhoneNumber.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhoneNumber.Location = New System.Drawing.Point(587, 186)
        Me.txtPhoneNumber.Name = "txtPhoneNumber"
        Me.txtPhoneNumber.Size = New System.Drawing.Size(276, 28)
        Me.txtPhoneNumber.TabIndex = 22
        '
        'lblPostalcode
        '
        Me.lblPostalcode.AutoSize = True
        Me.lblPostalcode.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostalcode.Location = New System.Drawing.Point(474, 141)
        Me.lblPostalcode.Name = "lblPostalcode"
        Me.lblPostalcode.Size = New System.Drawing.Size(81, 18)
        Me.lblPostalcode.TabIndex = 21
        Me.lblPostalcode.Text = "Postal Code"
        '
        'txtPostalCode
        '
        Me.txtPostalCode.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostalCode.Location = New System.Drawing.Point(587, 137)
        Me.txtPostalCode.Name = "txtPostalCode"
        Me.txtPostalCode.Size = New System.Drawing.Size(276, 28)
        Me.txtPostalCode.TabIndex = 20
        '
        'lblCountry
        '
        Me.lblCountry.AutoSize = True
        Me.lblCountry.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(474, 83)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(57, 18)
        Me.lblCountry.TabIndex = 19
        Me.lblCountry.Text = "Country"
        '
        'txtCountry
        '
        Me.txtCountry.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCountry.Location = New System.Drawing.Point(587, 79)
        Me.txtCountry.Name = "txtCountry"
        Me.txtCountry.Size = New System.Drawing.Size(276, 28)
        Me.txtCountry.TabIndex = 18
        '
        'lblProvince
        '
        Me.lblProvince.AutoSize = True
        Me.lblProvince.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvince.Location = New System.Drawing.Point(474, 26)
        Me.lblProvince.Name = "lblProvince"
        Me.lblProvince.Size = New System.Drawing.Size(62, 18)
        Me.lblProvince.TabIndex = 17
        Me.lblProvince.Text = "Province"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(474, 244)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(57, 18)
        Me.lblEmail.TabIndex = 16
        Me.lblEmail.Text = "Email Id"
        '
        'txtEmailId
        '
        Me.txtEmailId.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailId.Location = New System.Drawing.Point(587, 239)
        Me.txtEmailId.Name = "txtEmailId"
        Me.txtEmailId.Size = New System.Drawing.Size(276, 28)
        Me.txtEmailId.TabIndex = 15
        '
        'txtProvince
        '
        Me.txtProvince.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProvince.Location = New System.Drawing.Point(587, 23)
        Me.txtProvince.Name = "txtProvince"
        Me.txtProvince.Size = New System.Drawing.Size(276, 28)
        Me.txtProvince.TabIndex = 14
        Me.tltipProvince.SetToolTip(Me.txtProvince, "Province")
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(13, 294)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(46, 18)
        Me.lblStatus.TabIndex = 25
        Me.lblStatus.Text = "Status"
        '
        'txtStatus
        '
        Me.txtStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtStatus.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatus.ForeColor = System.Drawing.Color.Red
        Me.txtStatus.Location = New System.Drawing.Point(110, 291)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(426, 21)
        Me.txtStatus.TabIndex = 24
        '
        'tltipProvince
        '
        Me.tltipProvince.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        '
        'lblStreetNumberErr
        '
        Me.lblStreetNumberErr.AutoSize = True
        Me.lblStreetNumberErr.ForeColor = System.Drawing.Color.Red
        Me.lblStreetNumberErr.Location = New System.Drawing.Point(113, 218)
        Me.lblStreetNumberErr.Name = "lblStreetNumberErr"
        Me.lblStreetNumberErr.Size = New System.Drawing.Size(60, 13)
        Me.lblStreetNumberErr.TabIndex = 26
        Me.lblStreetNumberErr.Text = "Street Error"
        Me.lblStreetNumberErr.Visible = False
        '
        'lblProvinceErr
        '
        Me.lblProvinceErr.AutoSize = True
        Me.lblProvinceErr.ForeColor = System.Drawing.Color.Red
        Me.lblProvinceErr.Location = New System.Drawing.Point(589, 57)
        Me.lblProvinceErr.Name = "lblProvinceErr"
        Me.lblProvinceErr.Size = New System.Drawing.Size(74, 13)
        Me.lblProvinceErr.TabIndex = 27
        Me.lblProvinceErr.Text = "Province Error"
        Me.lblProvinceErr.Visible = False
        '
        'lblFirstNameErr
        '
        Me.lblFirstNameErr.AutoSize = True
        Me.lblFirstNameErr.ForeColor = System.Drawing.Color.Red
        Me.lblFirstNameErr.Location = New System.Drawing.Point(112, 112)
        Me.lblFirstNameErr.Name = "lblFirstNameErr"
        Me.lblFirstNameErr.Size = New System.Drawing.Size(82, 13)
        Me.lblFirstNameErr.TabIndex = 28
        Me.lblFirstNameErr.Text = "First Name Error"
        Me.lblFirstNameErr.Visible = False
        '
        'lblLastNameErr
        '
        Me.lblLastNameErr.AutoSize = True
        Me.lblLastNameErr.ForeColor = System.Drawing.Color.Red
        Me.lblLastNameErr.Location = New System.Drawing.Point(113, 167)
        Me.lblLastNameErr.Name = "lblLastNameErr"
        Me.lblLastNameErr.Size = New System.Drawing.Size(83, 13)
        Me.lblLastNameErr.TabIndex = 29
        Me.lblLastNameErr.Text = "Last Name Error"
        Me.lblLastNameErr.Visible = False
        '
        'lblCityErr
        '
        Me.lblCityErr.AutoSize = True
        Me.lblCityErr.ForeColor = System.Drawing.Color.Red
        Me.lblCityErr.Location = New System.Drawing.Point(113, 268)
        Me.lblCityErr.Name = "lblCityErr"
        Me.lblCityErr.Size = New System.Drawing.Size(49, 13)
        Me.lblCityErr.TabIndex = 30
        Me.lblCityErr.Text = "City Error"
        Me.lblCityErr.Visible = False
        '
        'lblCountryErr
        '
        Me.lblCountryErr.AutoSize = True
        Me.lblCountryErr.ForeColor = System.Drawing.Color.Red
        Me.lblCountryErr.Location = New System.Drawing.Point(589, 110)
        Me.lblCountryErr.Name = "lblCountryErr"
        Me.lblCountryErr.Size = New System.Drawing.Size(68, 13)
        Me.lblCountryErr.TabIndex = 31
        Me.lblCountryErr.Text = "Country Error"
        Me.lblCountryErr.Visible = False
        '
        'lblPostalCodeErr
        '
        Me.lblPostalCodeErr.AutoSize = True
        Me.lblPostalCodeErr.ForeColor = System.Drawing.Color.Red
        Me.lblPostalCodeErr.Location = New System.Drawing.Point(589, 168)
        Me.lblPostalCodeErr.Name = "lblPostalCodeErr"
        Me.lblPostalCodeErr.Size = New System.Drawing.Size(89, 13)
        Me.lblPostalCodeErr.TabIndex = 32
        Me.lblPostalCodeErr.Text = "Postal Code Error"
        Me.lblPostalCodeErr.Visible = False
        '
        'lblPhoneNumberErr
        '
        Me.lblPhoneNumberErr.AutoSize = True
        Me.lblPhoneNumberErr.ForeColor = System.Drawing.Color.Red
        Me.lblPhoneNumberErr.Location = New System.Drawing.Point(590, 218)
        Me.lblPhoneNumberErr.Name = "lblPhoneNumberErr"
        Me.lblPhoneNumberErr.Size = New System.Drawing.Size(103, 13)
        Me.lblPhoneNumberErr.TabIndex = 33
        Me.lblPhoneNumberErr.Text = "Phone Number Error"
        Me.lblPhoneNumberErr.Visible = False
        '
        'lblEmailAddressErr
        '
        Me.lblEmailAddressErr.AutoSize = True
        Me.lblEmailAddressErr.ForeColor = System.Drawing.Color.Red
        Me.lblEmailAddressErr.Location = New System.Drawing.Point(589, 270)
        Me.lblEmailAddressErr.Name = "lblEmailAddressErr"
        Me.lblEmailAddressErr.Size = New System.Drawing.Size(68, 13)
        Me.lblEmailAddressErr.TabIndex = 34
        Me.lblEmailAddressErr.Text = "Email id Error"
        Me.lblEmailAddressErr.Visible = False
        '
        'ContactForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(954, 641)
        Me.Controls.Add(Me.lblEmailAddressErr)
        Me.Controls.Add(Me.lblPhoneNumberErr)
        Me.Controls.Add(Me.lblPostalCodeErr)
        Me.Controls.Add(Me.lblCountryErr)
        Me.Controls.Add(Me.lblCityErr)
        Me.Controls.Add(Me.lblLastNameErr)
        Me.Controls.Add(Me.lblFirstNameErr)
        Me.Controls.Add(Me.lblProvinceErr)
        Me.Controls.Add(Me.lblStreetNumberErr)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.lblStreetNumber)
        Me.Controls.Add(Me.txtStreetNumber)
        Me.Controls.Add(Me.lblLastName)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.lblFirstName)
        Me.Controls.Add(Me.lblPrimaryKey)
        Me.Controls.Add(Me.tb_index)
        Me.Controls.Add(Me.txtFirstName)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.txtStatus)
        Me.Controls.Add(Me.lblCity)
        Me.Controls.Add(Me.lblPhoneNumber)
        Me.Controls.Add(Me.txtPhoneNumber)
        Me.Controls.Add(Me.lblPostalcode)
        Me.Controls.Add(Me.txtPostalCode)
        Me.Controls.Add(Me.lblCountry)
        Me.Controls.Add(Me.txtCountry)
        Me.Controls.Add(Me.lblProvince)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.txtEmailId)
        Me.Controls.Add(Me.txtProvince)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btn_next)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ContactForm"
        Me.Text = "Customer Detail Form"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CsvDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_next As Button
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents btnPrev As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents tb_index As TextBox
    Friend WithEvents lblPrimaryKey As Label
    Friend WithEvents lblFirstName As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblForImport As Label
    Friend WithEvents txtboxBrowse As TextBox
    Friend WithEvents btnBrowser As Button
    Friend WithEvents lblLastName As Label
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents lblCity As Label
    Friend WithEvents txtCity As TextBox
    Friend WithEvents lblStreetNumber As Label
    Friend WithEvents txtStreetNumber As TextBox
    Friend WithEvents lblPhoneNumber As Label
    Friend WithEvents txtPhoneNumber As TextBox
    Friend WithEvents lblPostalcode As Label
    Friend WithEvents txtPostalCode As TextBox
    Friend WithEvents lblCountry As Label
    Friend WithEvents txtCountry As TextBox
    Friend WithEvents lblProvince As Label
    Friend WithEvents lblEmail As Label
    Friend WithEvents txtEmailId As TextBox
    Friend WithEvents txtProvince As TextBox
    Friend WithEvents CsvDataGrid As DataGridView
    Friend WithEvents lblCsvFileData As Label
    Friend WithEvents btnUpload As Button
    Friend WithEvents lblStatus As Label
    Friend WithEvents txtStatus As TextBox
    Friend WithEvents tltipProvince As ToolTip
    Friend WithEvents tltpBtnBrowse As ToolTip
    Friend WithEvents tltpBtnPrev As ToolTip
    Friend WithEvents tltpBtnUpload As ToolTip
    Friend WithEvents tltpBtnNext As ToolTip
    Friend WithEvents lblStreetNumberErr As Label
    Friend WithEvents lblProvinceErr As Label
    Friend WithEvents lblFirstNameErr As Label
    Friend WithEvents lblLastNameErr As Label
    Friend WithEvents lblCityErr As Label
    Friend WithEvents lblCountryErr As Label
    Friend WithEvents lblPostalCodeErr As Label
    Friend WithEvents lblPhoneNumberErr As Label
    Friend WithEvents lblEmailAddressErr As Label
    Friend WithEvents btnClearDbTable As Button
End Class
