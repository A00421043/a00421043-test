Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class CustomerModel
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=CustomerModel")
    End Sub

    Public Overridable Property Customers As DbSet(Of Customer)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.firstName) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.lastName) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.streetNumber) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.city) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.province) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.country) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.postalCode) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.phoneNumber) _
            .IsUnicode(False)

        modelBuilder.Entity(Of Customer)() _
            .Property(Function(e) e.emailAddress) _
            .IsUnicode(False)
    End Sub
End Class
