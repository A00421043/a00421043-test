Assignment #1
-------------------------------------------------------------------

The goal of the assignment is to simply combine all 5 of these programs into a single
program that will recursively read a series of data files in CSV format and enter 
them into a single CSV file.

The program must log the amount of time it takes to read the files in each directory 
and the time it takes to write the files to a file using the logger.
-------------------
Command to run this file in terminal
javac -cp .:"/Users/nitinakash/git/mcda5510_assignments/JavaSamples/lib/*" DirWalker.java
java -cp .:"/Users/nitinakash/git/mcda5510_assignments/JavaSamples/lib/*" DirWalker
------------------------------------------------------------------------------------------