import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class DirWalker {
	private SimpleCsvParser simpleCsvParser=new SimpleCsvParser();
	private static List<CSVRecord> outputRecord=new ArrayList<CSVRecord>();
	//Valid record Data set
	private static final String outputFileName = "output.csv";
	private static CSVPrinter csvPrinter;
	static BufferedWriter writer;
	//To get count of total invalid rows in all files.
	public static int totolRowSkipped=0;
	
	//To get count of total valid rows in all files.
	public static int totolValidRows=0;
    public void walk( String path ) throws IOException {
        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                csvPrinter.printRecords(outputRecord);
                csvPrinter.flush();
                outputRecord.clear();
            }
            else {
            	outputRecord.addAll(simpleCsvParser.readFile(f.getAbsoluteFile().toString()));
            }
            
        }
        
    }

    public static void main(String[] args) throws IOException  {
	    	//Capturing start time of the application
	    	final long startTime = System.currentTimeMillis();
	    	
    		//Check if output directory is present if not create a directory
    		checkAndCreateOutputDirectory();
    		
    		//Initiate a logger
    		SimpleLogging.LogIntoLogger();
    		writer = Files.newBufferedWriter(Paths.get("Output/"+outputFileName));
	    	
    		//Create a writer with header format of excel sheet.
	    	csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
	                .withHeader("First Name","Last Name","Street Number	","Street",	"City","Province","Postal Code","Country","Phone Number","email Address"));
	    
	    	DirWalker fw = new DirWalker();
	    	//To call walk function with relative path of sample data
	    	fw.walk((System.getProperty("user.dir"))+"/Sample Data" );
	    	//To close csvPrinter after data has been written to output file.
	    	csvPrinter.close();
	    System.out.println(outputRecord.size());
		final long endTime = System.currentTimeMillis();
		SimpleLogging.logger.log(Level.INFO,"Total valide record: " +totolValidRows+"\n"+"Total Invalid record: "+totolRowSkipped+"\n"+"Total execution time: " + (endTime - startTime) + " ms");
    }
    
  //To Create a output directory if not present 
    public static void checkAndCreateOutputDirectory()
    {
    		File outputDir=new File("Output");
    		if (!outputDir.exists()) {
    		    try{
    		    		outputDir.mkdir();
    		        } 
    		    catch(SecurityException se){
    		        //handle it
    		    		se.printStackTrace();
    		    }        
    		}
    }
}