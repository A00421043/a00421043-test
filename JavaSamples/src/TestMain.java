import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class TestMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String path="/Users/nitinakash/git/mcda5510_assignments/JavaSamples/Sample Data";
        walk("/Users/nitinakash/git/mcda5510_assignments/JavaSamples/Sample Data" );
         
	}
	
	public static void walk( String path ) {

        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
                System.out.println( "File:" + f.getAbsoluteFile() );
                //Calling function to readFile
                readFile(f.getAbsoluteFile().toString());
            }
        }
	}
	
	public static void  readFile(String filePath) {
		Reader in;
		try {
			in = new FileReader(filePath);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			for (CSVRecord record : records) {
				
				for(int i=0; i<record.size();i++ )
				{
					System.out.print(record.get(i)+"\t");
				}
				System.out.println();
			   
			}			
			
		} catch ( IOException e) {
			e.printStackTrace();
		}
	}
}
