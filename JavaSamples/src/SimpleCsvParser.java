import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SimpleCsvParser {

	public  List<CSVRecord> readFile(String filePath)
	{
		int index = 0;
		int errorCheck=0;
		String errorMessage;
		List<CSVRecord> validRecord=new ArrayList<CSVRecord>();
		String[] columnName= {"First Name","Last Name","Street Number	","Street",	"City","Province","Postal Code","Country","Phone Number","email Address"};
		Reader in;
		try {
			in = new FileReader(filePath);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			for (CSVRecord record : records) {
				for(int i=0; i<record.size();i++ )
				{
					//For header-->Save index in array
					if(index==0)
					{
						columnName[i]=record.get(i);
					}
					else
					{
						
						if(record.get(i)=="" || record.get(i) == null || record.get(i).isEmpty())
						{
							errorCheck=1;
							errorMessage="Column Name: "+columnName[i]+ "  Value is Null in Line  no:"+(index+1)+" File Path: "+ filePath+" File Name"+new File(filePath).getName().toString();
							SimpleLogging.logger.log(Level.INFO,errorMessage);
						}
						
					}
				}
				if(errorCheck==0 && index!=0)
				{
					validRecord.add(record);
					++DirWalker.totolValidRows;
				}
				else if(errorCheck==1)
				{
					++DirWalker.totolRowSkipped ;
				}
				index=index+1;
				errorCheck=0;
			}			
			
		} catch ( IOException e) {
			e.printStackTrace();
		}
		return validRecord;
	}
}
