﻿//  File ContactGenerator
//  Sample code was taken from:
//  http://www.csharpprogramming.tips/2013/06/RandomDoxGenerator.html
//  Other useful methods are there.
//
// Requirements:
// Exapand on the below example to create a CSV file (https://en.wikipedia.org/wiki/Comma-separated_values)
// For contacts with the following data
// First Name
// Last Name
// Street Number
// City
// Province
// Country  == Canada ( Simply insert "canada")
// Postal Code  ( they can be read form a file for this example if you choose, or generate if you wish)
// Phone Number ( they can be read form a file for this example if you choose, or generate if you wish)
// email Address ( Append firstname.lastname against a series for domain names read for a file
//
// Please always try to write clean and readable code
// Here is a good reference doc http://ricardogeek.com/docs/clean_code.html  
// Submit to Bitbucket under Assignment1
// 
using System;
using System.IO;
// Describes what is a namespace 
// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/namespaces/
namespace MSCDA5510
{
    class ContactGenerator
    {
        // instance of random number generator
        Random rand = new Random();
        String path = @"C:\Users\SMU MScCDA\Desktop\Projects\Assign1\";
        static int addressPointer;
        static int lineNoOfCity;
        string city;
        static void Main(string[] args)
        {
            // instance of ContactGenerator
            ContactGenerator dg = new ContactGenerator();
        }
        public ContactGenerator()
        {
            String firstName;
            String lastName;
            String COMMA = ",";
            String outputFileName = path+@"customers.csv";
            if (File.Exists(outputFileName))
            {
                Console.Write(" File " + outputFileName + " exists, appending");
            }
            StreamWriter fileStream = new StreamWriter(outputFileName, true);
            // Write Header
            fileStream.Write("First Name");
            fileStream.Write(COMMA);
            fileStream.Write("Last Name");
            fileStream.Write(COMMA);
            fileStream.Write("Street Number");
            fileStream.Write(COMMA);
            fileStream.Write("City");
            fileStream.Write(COMMA);
            fileStream.Write("Province");
            fileStream.Write(COMMA);
            fileStream.Write("Country");
            fileStream.Write(COMMA);
            fileStream.Write("Postal Code");
            fileStream.Write(COMMA);
            fileStream.Write("Phone number");
            fileStream.Write(COMMA);
            fileStream.Write("Email Address");
            fileStream.WriteLine();
            for (int i = 0; i < 20; i++)
            {
                firstName = GenerateFirstName();
                lastName = GenerateLastName();
                fileStream.Write(firstName);
                fileStream.Write(COMMA);
                fileStream.Write(lastName);
                //Added for updating Street Number,City,province,country,Postal code,Phone Number and email Id to CSV file.
                fileStream.Write(COMMA);
                fileStream.Write(GenerateStreetNumber());

                fileStream.Write(COMMA);
                fileStream.Write(GenerateCity());

                fileStream.Write(COMMA);
                fileStream.Write(GenerateProvince());

                fileStream.Write(COMMA+ "CANADA");

                fileStream.Write(COMMA);
                fileStream.Write(GeneratePostalCode());

                fileStream.Write(COMMA);
                fileStream.Write(GeneratePhoneNumber());
                
                fileStream.Write(COMMA);
                fileStream.Write(GenerateEmailId(firstName, lastName));
                fileStream.WriteLine();
            }
            fileStream.Close();
        }
        //For generating Email address.
        public string GenerateEmailId(String firstName, String lastName)
        {
            String domain = path + "emails.txt";

            return (firstName + "." + lastName + "@"+ReturnRandomLine(domain));
        }

        public string GeneratePhoneNumber()
        {
            //String phoneNumber =path+"phoneNumbers.txt";
            String phoneNumber = rand.Next(900, 999) + "-" + rand.Next(1, 999).ToString().PadLeft(3,'0') +"-"+ rand.Next(1, 9999).ToString().PadLeft(4,'0');
            return phoneNumber;
           // return ReturnRandomLine(phoneNumber);
        }
        public string GeneratePostalCode()
        {
            
            String pincode=path+"postalCodes.txt";
            //return ReturnRandomLine(pincode);
            return GenerateAddress(pincode, lineNoOfCity);
        }

        public string GenerateProvince()
        {
            String province =path+"provinces.txt";
            //return ReturnRandomLine(province);
            return GenerateAddress(province, lineNoOfCity);
        }
        public string GenerateFirstName()
        {
            String firstNames = path+@"firstNames.txt";
            return ReturnRandomLine(firstNames);
        }
        public string GenerateLastName()
        {
            String lastNames = path+"lastNames.txt";
            return ReturnRandomLine(lastNames);
        }

        public int GenerateStreetNumber()
        {
            return rand.Next(1, 100);
        }
        public string GenerateCity()
        {
            String city = path+"city.txt";
            return ReturnRandomLine(city);
        }
        // Gets a line from a file
        public string ReturnRandomLine(string FileName)
        {
            string sReturn = string.Empty;
            using (FileStream myFile = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader myStream = new StreamReader(myFile))
                {
                    // just cast it to int because we know it will be less than 
                    int fileLength = (int)myFile.Length;

                    addressPointer = rand.Next(1, fileLength);
                    // Seek file stream pointer to a rand position...
                    myStream.BaseStream.Seek(addressPointer, SeekOrigin.Begin);
                    // Read the rest of that line.
                    myStream.ReadLine();
                    
                    // Return the next, full line...
                    sReturn = myStream.ReadLine();
                    //To keep track of city of customer
                    if (FileName.Equals(@"C:\Users\SMU MScCDA\Desktop\Projects\Assign1\city.txt") && !System.String.IsNullOrWhiteSpace(sReturn))
                    {
                        city = File.ReadAllText(FileName);
                        lineNoOfCity = city.Substring(1, addressPointer).Split('\n').Length;

                    }

                }
            }
            // If our random file position was too close to the end of the file, it will return an empty string
            // I avoided a while loop in the case that the file is empty or contains only one line
            if (System.String.IsNullOrWhiteSpace(sReturn))
            {
                sReturn = ReturnRandomLine(FileName);
            }
            return sReturn;
        }

        //To Generate matching address postalcode and province according to city for customer
        public string GenerateAddress(string FileName,int positionOfData)
        {
            String sReturn;
            //For city record matching province and city
            using (FileStream myFile = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                using(StreamReader myStream=new StreamReader(myFile))
                {
                    sReturn = myStream.ReadToEnd().ToString().Replace("\r", "").Split('\n')[positionOfData];
                    //sReturn=myStream.ReadToEnd().ToString().Split('\n')[positionOfData];
                }
            }
                return sReturn;
        }
    }
}