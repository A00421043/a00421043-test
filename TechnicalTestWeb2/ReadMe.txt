This solution is a simple web application that allows users to have a 
set of shipments associated with their customer account.

Please make a copy of this git repo in your repo called TechnicalTestWeb2
Please make updates in this file and to the code and push when complete( or end of class) 

Item 1: Setup..
There is script file located in the "SQL Script", directory.
Create a database and using SQL Management Console exedcute the script to populate the data.
Note: You will have to update the Web.config to reference your DB name/credentials
Open--->Run as a script.
Changed connection string in Web.config file.
 <add name="DataModel" connectionString="data source=DESKTOP-OMVE66E;initial catalog=MyTest;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework" providerName="System.Data.SqlClient" />
Item 2: 
There is one compilation error in the project fix it.
Answer: ServiceTypes is not a field of Shipment. Changed it to ServiceType
        in Shipment model
		public string ServiceType { get; set; }
What was it?  How did you fix?
Changed ServiceType
Item 3:
The Shipments link in the menu bar of the app is not working, 
it should like to the index page of the ShipmentsController 
What was it?  How did you fix?

Changed in _Layout.cshtml file from shippppment to shipment controller
                    <li>@Html.ActionLink("Shipments", "Index", "Shipppments")</li>
					to
					<li>@Html.ActionLink("Shipments", "Index", "Shipments")</li>
Item 4:
Make the name label for the Customer on the shipment object say "Customer Name" in all places

Please Answer:
What was required to fix?
Added anotation [Display(Name="Customer name" )] to shipment model
and changed the edit page razor from customer Id--->Customer Name
            @Html.LabelFor(model => model. CustomerID, "Customer Name", htmlAttributes: new { @class = "control-label col-md-2" })
Item 5:
There is a bug in the code.
The estimated ship date must be at least 24 hours after the Date Ordered.  Fix

Please Answer:
Changed validation class business logic.
int result = DateTime.Compare(estShipDate, orderPlacedDate.AddHours(24) );             
if (result < 0)             
{                 
	return new ValidationResult("The estimated ship date must be at least 24 hours after the Date Ordered");             
}

What was wrong and what was required to fix?

Item 6:
When Editing customer Nitin, the province appear as Quebec.  Not Ontario - the default for create.

Explain why it was not Ontario or NS?
customer.provinces = GetSelectListItems(provinces);---It is getting customer provience which is there in the list but for Nitin provience is NS 
which is not on the list so in this is the case default is for the list is Qubeck and not Ontorio( Which is the default when we create customer) which get populated for Nitin

Item 7:
Add a button to the right of the customer dropdown to open the add a new customer.
Hint: you can use @Html.ActionLink ( does not have to be a button)
@Html.ActionLink("Create New Customer","Create","Customers")

Item 8:
Convert Text dates to date picker
[DataType(DataType.Date)]
[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]